
-- SUMMARY --

Karmacracy is a democratic system that measures the audience of people by
counting clicks from the links they share on social networks. When a link is
shared, it is replaced by a link to a shortener. All the traffic generated
through this shortener is bound to the person that created the link; thus,
counting kclicks (clicks that carry karma).

This module provides an integration between Drupal and Karmacracy.

-- FEATURES --

* Allows you to insert the Karmacracy widget in your Drupal site in an easy way.

-- INSTALLATION --

* Install Karmacracy module as usual on your Drupal site.

-- CONFIGURATION --

* Configure widget settings in Administration >> Configuration >> Web services
  >> Karmacracy

-- CREDITS --

Author and maintainer:
* Sergio Martín Morillas <smartinmorillas@gmail.com>
  http://drupal.org/user/191570

This module is not made by the creators of Karmacracy.