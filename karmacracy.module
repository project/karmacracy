<?php

/**
 * @file
 * Provides an integration between Drupal and Karmacracy.
 */

define('KARMACRACY_WIDGET_VERSION', '1.3');

/**
 * Implements hook_help().
 */
function karmacracy_help($path, $arg) {
  switch ($path) {
    case 'admin/help#karmacracy':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Karmacracy is a democratic system that measures the audience of people by counting clicks from the links they share on social networks. When a link is shared, it is replaced by a link to a shortener. All the traffic generated through this shortener is bound to the person that created the link; thus, counting kclicks (clicks that carry karma). For more information about Karmacracy, go to <a href="@karmacracy">the official site</a>.', array('@karmacracy' => 'http://www.karmacracy.com/')) . '</p>';
      $output .= '<p>' . t('This module provides an integration between Drupal and Karmacracy.') . '</p>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Allows you to insert the <a href="@widget">Karmacracy widget</a> in your Drupal site in an easy way.', array('@widget' => 'http://karmacracy.com/sections/widget/introduction/get-your-widget.php')) . '</li>';
      $output .= '</ul>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Karmacracy widget') . '</dt>';
      $output .= '<dd>' . t('Users with the <em>Administer site configuration</em> permission can enable and configure Karmacracy widget from the <a href="@karmacracy">Karmacracy administration</a> page, or alternately enable the <em>Karmacracy widget</em> block on the <a href="@blocks">Blocks administration</a> page.', array('@karmacracy' => url('admin/config/services/karmacracy'), '@blocks' => url('admin/structure/block'))) . '</dd>';
      $output .= '</dl>';
      return $output;
    case 'admin/config/services/karmacracy/keys':
      $output = '<p>' . t('The Karmacracy third-party key is used to generate short URLs using the url shortener service of Karmacracy.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function karmacracy_menu() {
  $items['admin/config/services/karmacracy'] = array(
    'title' => 'Karmacracy',
    'description' => 'Configure the Karmacracy widget settings and third-party key.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('karmacracy_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'karmacracy.admin.inc',
    'weight' => -10,
  );
  $items['admin/config/services/karmacracy/widget'] = array(
    'title' => 'Widget',
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/services/karmacracy/keys'] = array(
    'title' => 'Third-party key',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('karmacracy_admin_keys_form'),
    'access arguments' => array('administer site configuration'),
    'weight' => 10,
    'file' => 'karmacracy.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function karmacracy_theme() {
  return array(
    'karmacracy_widget_html' => array(
      'variables' => array('id' => NULL, 'url' => NULL, 'options' => array()),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function karmacracy_block_info() {
  $blocks['karmacracy'] = array(
    'info' => t('Karmacracy widget'),
    //'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function karmacracy_block_view($delta = '') {
  if ($node = menu_get_object()) {
    $id = $node->nid;
  }
  else {
    $id = 'ID';
  }
  $path = drupal_is_front_page() ? '<front>' : current_path();
  $url = url($path, array('absolute' => TRUE));
  $options = variable_get('karmacracy_widget_settings', karmacracy_widget_default());

  $data['subject'] = NULL;
  $data['content'] = theme('karmacracy_widget_html', array('id' => $id, 'url' => $url, 'options' => $options));
  return $data;
}

/**
 * Implements hook_node_view().
 */
function karmacracy_node_view($node, $view_mode) {
  $global_active = variable_get('karmacracy_widget_active', 1);
  if (variable_get('karmacracy_node_widget_' . $node->type, $global_active)) {
    if ($view_mode == 'full') {
      $global_location = variable_get('karmacracy_widget_location', 'body');
      $location = variable_get('karmacracy_node_widget_location_' . $node->type, $global_location);
      if (($location == 'body' || $location == 'beforebody') && empty($node->in_preview)) {
        $url = url('node/' . $node->nid, array('absolute' => TRUE));
        $options = variable_get('karmacracy_widget_settings', karmacracy_widget_default());
        $node->content['karmacracy_widget'] = array(
          '#markup' => theme('karmacracy_widget_html', array('id' => $node->nid, 'url' => $url, 'options' => $options)),
          '#weight' => $location == 'body' ? 10 : -10,
        );
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds Karmacracy options to the node type form.
 */
function karmacracy_form_node_type_form_alter(&$form, $form_state) {
  $type = $form['#node_type'];
  $form['karmacracy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Karmacracy settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'karmacracy') . '/karmacracy.js'),
    ),
    '#group' => 'additional_settings',
  );
  $global_active = variable_get('karmacracy_widget_active', 1);
  $form['karmacracy']['karmacracy_node_widget'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Karmacracy widget.'),
    '#default_value' => variable_get('karmacracy_node_widget_' . $type->type, $global_active),
    '#description' => t('Add the Karmacracy widget in content of this type.'),
  );

  $global_location = variable_get('karmacracy_widget_location', 'block');
  $form['karmacracy']['karmacracy_node_widget_location'] = array(
    '#type' => 'select',
    '#title' => t('Widget location'),
    '#description' => t('Choose where you want to put the widget.'),
    '#default_value' => variable_get('karmacracy_node_widget_location_' . $type->type, $global_location),
    '#options' => array(
      'body' => t('After the body'),
      'beforebody' => t('Before the body'),
    ),
    '#states' => array(
      'invisible' => array(
        ':input[name="karmacracy_node_widget"]' => array('checked' => FALSE),
      ),
    ),
  );
}

function theme_karmacracy_widget_html($variables) {
  $id = $variables['id'];
  $url = $variables['url'];
  $options = $variables['options'];
  $options += karmacracy_widget_default();

  $kcyjsurl = 'http://rodney.karmacracy.com/widget-'. KARMACRACY_WIDGET_VERSION . '/?id=' . $id;
  $kcyjsurl .= "&type=h";
  $kcyjsurl .= "&width=" . $options["width"];
  $kcyjsurl .= "&sc=" . $options["sc"];
  $kcyjsurl .= "&rb=" . $options["rb"];
  $kcyjsurl .= "&np=" . $options["np"];
  $kcyjsurl .= "&c1=" . ltrim($options["color1"], '#');
  $kcyjsurl .= "&c2=" . ltrim($options["color2"], '#');
  $kcyjsurl .= "&c3=" . ltrim($options["color3"], '#');
  $kcyjsurl .= "&c4=" . ltrim($options["color4"], '#');
  $kcyjsurl .= "&c5=" . ltrim($options["color5"], '#');
  $kcyjsurl .= "&c6=" . ltrim($options["color6"], '#');
  $kcyjsurl .= "&c7=" . ltrim($options["color7"], '#');
  $kcyjsurl .= "&c8=" . ltrim($options["color8"], '#');
  $kcyjsurl .= "&c9=" . ltrim($options["color9"], '#');
  $kcyjsurl .= "&url=" . $url;

  return "<div class=\"kcy_karmacracy_widget_h_$id\"></div><script defer=\"defer\" src=\"$kcyjsurl\"></script>";
}

/**
 * Implements hook_shorten_service().
 */
function karmacracy_shorten_service() {
  $services = array();
  $services['kcy.me'] = array(
    'url' => 'http://kcy.me/api/?u=' . variable_get('karmacracy_user', '') . '&key=' . variable_get('karmacracy_keypass', '') . '&url=',
  );
  return $services;
}

function karmacracy_widget_default() {
  return array(
    'width' => '700',
    'sc' => 1,
    'rb' => 1,
    'np' => 0,
    'color1' => '#f2f2f2',
    'color2' => '#ffffff',
    'color3' => '#f2f2f2',
    'color4' => '#353535',
    'color5' => '#067dba',
    'color6' => '#ffffff',
    'color7' => '#353535',
    'color8' => '#AD1B25',
    'color9' => '#353535',
  );
}
